ACL=acl-2.2.53
ATTR=attr-2.4.48
#autoconf-2.69
#automake-1.16.2
BASH=bash-5.0
#bash-5.0-upstream_fixes-1.patch
BC=bc-3.1.5
BINUTILS=binutils-2.35
BISON=bison-3.7.1
#bzip2-1.0.8-install_docs-1.patch
BZIP2=bzip2-1.0.8
#check-0.15.2
#coreutils-8.32-i18n-1.patch
#coreutils-8.32
#dbus-1.12.20
DEJAGNU=dejagnu-1.6.2
#diffutils-3.7
#e2fsprogs-1.45.6
#elfutils-0.180
#eudev-3.2.9
#expat-2.2.9
EXPECT=expect5.45.4
FILE=file-5.39
#findutils-4.7.0
FLEX=flex-2.6.4
#gawk-5.1.0
GCC=gcc-10.2.0
#gdbm-1.18.1
GETTEXT=gettext-0.21
#glibc-2.32-fhs-1.patch
GLIBC=glibc-2.32
GMP=gmp-6.2.0
#gperf-3.1
GREP=grep-3.4
#groff-1.22.4
#grub-2.04
#gzip-1.10
IANA=iana-etc-20200821
#inetutils-1.9.4
#intltool-0.51.0
#iproute2-5.8.0
#kbd-2.3.0-backspace-1.patch
#kbd-2.3.0
#kmod-27
#less-551
#lfs-bootscripts-20200818
LIBCAP=libcap-2.42
#libffi-3.3
#libpipeline-1.5.3
#libtool-2.4.6
#linux-5.8.3
M4=m4-1.4.18
#make-4.3
#man-db-2.9.3
MAN_PAGES=man-pages-5.08
#meson-0.55.0
MPC=mpc-1.1.0
MPFR=mpfr-4.1.0
NCURSES=ncurses-6.2
#ninja-1.10.0
#openssl-1.1.1g
#patch-2.7.6
PERL=perl-5.32.0
PKG_CONFIG=pkg-config-0.29.2
#procps-ng-3.3.16
PSMISC=psmisc-23.3
#python-3.8.5-docs-html
PYTHON=Python-3.8.5
READLINE=readline-8.0
SED=sed-4.8
SHADOW=shadow-4.8.1
#sysklogd-1.5.1
#systemd-246
#systemd-man-pages-246
#sysvinit-2.97-consolidated-1.patch
#sysvinit-2.97
#tar-1.32
#tcl8.6.10-html
TCL=tcl8.6.10
TEXINFO=texinfo-6.7
#tzdata2020a
#udev-lfs-20171102
UTIL_LINUX=util-linux-2.36
#vim-8.2.1361
#XML-Parser-2.46
XZ=xz-5.2.5
ZLIB=zlib-1.2.11
ZSTD=zstd-1.4.5

cd $LFS/sources

############ MAN_PAGES ############
echo $MAN_PAGES" start"

tar -xf $MAN_PAGES.tar.xz
cd $MAN_PAGES

echo $MAN_PAGES" make"
make > /logs/$MAN_PAGES-make.log

cd /sources
rm -rf $MAN_PAGES

############ TCL ############
echo $TCL" start"

tar -xf $TCL-src.tar.gz
cd $TCL

tar -xf ../tcl8.6.10-html.tar.gz --strip-components=1

echo $TCL" config"
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            $([ "$(uname -m)" = x86_64 ] && echo --enable-64bit) > /logs/$TCL-config.log

echo $TCL" make"
make > /logs/$TCL-make.log
echo $TCL" make install"
sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.1|/usr/lib/tdbc1.1.1|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1|/usr/include|"            \
    -i pkgs/tdbc1.1.1/tdbcConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.0|/usr/lib/itcl4.2.0|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.0/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.0|/usr/include|"            \
    -i pkgs/itcl4.2.0/itclConfig.sh

unset SRCDIR
make install > /logs/$TCL-make_install.log

chmod -v u+w /usr/lib/libtcl8.6.so

make install-private-headers

ln -sfv tclsh8.6 /usr/bin/tclsh

cd /sources
rm -rf $TCL

############ EXPECT ############
echo $EXPECT" start"

tar -xf $EXPECT.tar.gz
cd $EXPECT

echo $EXPECT" config"
./configure --prefix=/usr           \
            --with-tcl=/usr/lib     \
            --enable-shared         \
            --mandir=/usr/share/man \
            --with-tclinclude=/usr/include > /logs/$EXPECT-config.log

echo $EXPECT" make"
make > /logs/$EXPECT-make.log
echo $EXPECT" make install"
make install > /logs/$EXPECT-make_install.log

ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib

cd /sources
rm -rf $EXPECT

############ DEJAGNU ############
echo $DEJAGNU" start"

tar -xf $DEJAGNU.tar.gz
cd $DEJAGNU

echo $DEJAGNU" config"
./configure --prefix=/usr > /logs/$DEJAGNU-config.log

makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi
makeinfo --plaintext       -o doc/dejagnu.txt  doc/dejagnu.texi

echo $DEJAGNU" make install"
make install > /logs/$DEJAGNU-make_install.log
install -v -dm755  /usr/share/doc/dejagnu-1.6.2
install -v -m644   doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.2

cd /sources
rm -rf $DEJAGNU

############ IANA ############
echo $IANA" start"

tar -xf $IANA.tar.gz
cd $IANA

echo $IANA" make install"
cp services protocols /etc > /logs/$IANA-make_install.log

cd /sources
rm -rf $IANA

############ GLIBC ############
echo $GLIBC" start"

tar -xf $GLIBC.tar.xz
cd $GLIBC

patch -Np1 -i ../glibc-2.32-fhs-1.patch

mkdir -v build
cd       build

echo $GLIBC" config"
../configure --prefix=/usr                            \
             --disable-werror                         \
             --enable-kernel=3.2                      \
             --enable-stack-protector=strong          \
             --with-headers=/usr/include              \
             libc_cv_slibdir=/lib > /logs/$GLIBC-config.log

echo $GLIBC" make"
make > /logs/$GLIBC-make.log

case $(uname -m) in
  i?86)   ln -sfnv $PWD/elf/ld-linux.so.2        /lib ;;
  x86_64) ln -sfnv $PWD/elf/ld-linux-x86-64.so.2 /lib ;;
esac

echo $GLIBC" check"
make check > /logs/$GLIBC-check.log

touch /etc/ld.so.conf

sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile

echo $GLIBC" make install"
make install > /logs/$GLIBC-make_install.log

cp -v ../nscd/nscd.conf /etc/nscd.conf
mkdir -pv /var/cache/nscd

mkdir -pv /usr/lib/locale
localedef -i POSIX -f UTF-8 C.UTF-8 2> /dev/null || true
localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8
localedef -i de_DE -f ISO-8859-1 de_DE
localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro
localedef -i de_DE -f UTF-8 de_DE.UTF-8
localedef -i el_GR -f ISO-8859-7 el_GR
localedef -i en_GB -f UTF-8 en_GB.UTF-8
localedef -i en_HK -f ISO-8859-1 en_HK
localedef -i en_PH -f ISO-8859-1 en_PH
localedef -i en_US -f ISO-8859-1 en_US
localedef -i en_US -f UTF-8 en_US.UTF-8
localedef -i es_MX -f ISO-8859-1 es_MX
localedef -i fa_IR -f UTF-8 fa_IR
localedef -i fr_FR -f ISO-8859-1 fr_FR
localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
localedef -i it_IT -f ISO-8859-1 it_IT
localedef -i it_IT -f UTF-8 it_IT.UTF-8
localedef -i ja_JP -f EUC-JP ja_JP
localedef -i ja_JP -f SHIFT_JIS ja_JP.SIJS 2> /dev/null || true
localedef -i ja_JP -f UTF-8 ja_JP.UTF-8
localedef -i ru_RU -f KOI8-R ru_RU.KOI8-R
localedef -i ru_RU -f UTF-8 ru_RU.UTF-8
localedef -i tr_TR -f UTF-8 tr_TR.UTF-8
localedef -i zh_CN -f GB18030 zh_CN.GB18030
localedef -i zh_HK -f BIG5-HKSCS zh_HK.BIG5-HKSCS

make localedata/install-locales

cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF

tar -xf ../../tzdata2020a.tar.gz

ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}

for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward pacificnew systemv; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done

cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO

tzselect

ln -sfv /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib

EOF

cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
mkdir -pv /etc/ld.so.conf.d

cd /sources
rm -rf $GLIBC

############ ZLIB ############
echo $ZLIB" start"

tar -xf $ZLIB.tar.xz
cd $ZLIB

echo $ZLIB" config"
./configure --prefix=/usr > /logs/$ZLIB-config.log

echo $ZLIB" make"
make > /logs/$ZLIB-make.log
echo $ZLIB" make install"
make install > /logs/$ZLIB-make_install.log

mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libz.so) /usr/lib/libz.so

cd /sources
rm -rf $ZLIB

############ BZIP2 ############
echo $BZIP2" start"

tar -xf $BZIP2.tar.gz
cd $BZIP2

patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch

sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile

sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile

make -f Makefile-libbz2_so
make clean

echo $BZIP2" make"
make > /logs/$BZIP2-make.log
echo $BZIP2" make install"
make PREFIX=/usr install > /logs/$BZIP2-make_install.log

cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat

cd /sources
rm -rf $BZIP2

############ XZ ############
echo $XZ" start"

tar -xf $XZ.tar.xz
cd $XZ

echo $XZ" config"
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/xz-5.2.5 > /logs/$XZ-config.log

echo $XZ" make"
make > /logs/$XZ-make.log
echo $XZ" make install"
make install > /logs/$XZ-make_install.log

mv -v   /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
mv -v /usr/lib/liblzma.so.* /lib
ln -svf ../../lib/$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so

cd /sources
rm -rf $XZ

############ ZSTD ############
echo $ZSTD" start"

tar -xf $ZSTD.tar.gz
cd $ZSTD

echo $ZSTD" make"
make > /logs/$ZSTD-make.log
echo $ZSTD" make install"
make prefix=/usr install > /logs/$ZSTD-make_install.log

rm -v /usr/lib/libzstd.a
mv -v /usr/lib/libzstd.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so

cd /sources
rm -rf $ZSTD

############ FILE ############
echo $FILE" start"

tar -xf $FILE.tar.gz
cd $FILE

echo $FILE" config"
./configure --prefix=/usr > /logs/$FILE-config.log

echo $FILE" make"
make > /logs/$FILE-make.log
echo $FILE" make install"
make install > /logs/$FILE-make_install.log

cd /sources
rm -rf $FILE

############ READLINE ############
echo $READLINE" start"

tar -xf $READLINE.tar.gz
cd $READLINE

sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install

echo $READLINE" config"
./configure --prefix=/usr    \
            --disable-static \
            --with-curses    \
            --docdir=/usr/share/doc/readline-8.0 > /logs/$READLINE-config.log

echo $READLINE" make"
make SHLIB_LIBS="-lncursesw" > /logs/$READLINE-make.log
echo $READLINE" make install"
make SHLIB_LIBS="-lncursesw" install > /logs/$READLINE-make_install.log

mv -v /usr/lib/lib{readline,history}.so.* /lib
chmod -v u+w /lib/lib{readline,history}.so.*
ln -sfv ../../lib/$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
ln -sfv ../../lib/$(readlink /usr/lib/libhistory.so ) /usr/lib/libhistory.so

install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.0

cd /sources
rm -rf $READLINE

############ M4 ############
echo $M4" start"

tar -xf $M4.tar.xz
cd $M4

sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

echo $M4" config"
./configure --prefix=/usr > /logs/$M4-config.log

echo $M4" make"
make > /logs/$M4-make.log
echo $M4" make install"
make install > /logs/$M4-make_install.log

cd /sources
rm -rf $M4

############ BC ############
echo $BC" start"

tar -xf $BC.tar.xz
cd $BC

PREFIX=/usr CC=gcc CFLAGS="-std=c99" ./configure.sh -G -O3

echo $BC" make"
make > /logs/$BC-make.log
echo $BC" make install"
make install > /logs/$BC-make_install.log

cd /sources
rm -rf $BC

############ FLEX ############
echo $FLEX" start"

tar -xf $FLEX.tar.gz
cd $FLEX

echo $FLEX" config"
./configure --prefix=/usr --docdir=/usr/share/doc/flex-2.6.4 > /logs/$FLEX-config.log

echo $FLEX" make"
make > /logs/$FLEX-make.log
echo $FLEX" make install"
make install > /logs/$FLEX-make_install.log

ln -sv flex /usr/bin/lex

cd /sources
rm -rf $FLEX

############ BINUTILS ############
echo $BINUTILS" start"

tar -xf $BINUTILS.tar.xz
cd $BINUTILS

expect -c "spawn ls"

sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in

mkdir -v build
cd       build

echo $BINUTILS" config"
../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib > /logs/$BINUTILS-config.log

echo $BINUTILS" make"
make tooldir=/usr > /logs/$BINUTILS-make.log
echo $BINUTILS" check"
make -k check > /logs/$BINUTILS-check.log
echo $BINUTILS" make install"
make tooldir=/usr install > /logs/$BINUTILS-make_install.log

cd /sources
rm -rf $BINUTILS

############ GMP ############
echo $GMP" start"

tar -xf $GMP.tar.xz
cd $GMP

echo $GMP" config"
./configure --prefix=/usr    \
            --enable-cxx     \
            --disable-static \
            --docdir=/usr/share/doc/gmp-6.2.0 > /logs/$GMP-config.log

echo $GMP" make"
make > /logs/$GMP-make.log
make html
echo $GMP" check"
make check 2>&1 | tee gmp-check-log
awk '/# PASS:/{total+=$3} ; END{print total}' gmp-check-log
echo $GMP" make install"
make install > /logs/$GMP-make_install.log
make install-html

cd /sources
rm -rf $GMP

############ MPFR ############
echo $MPFR" start"

tar -xf $MPFR.tar.xz
cd $MPFR

echo $MPFR" config"
./configure --prefix=/usr        \
            --disable-static     \
            --enable-thread-safe \
            --docdir=/usr/share/doc/mpfr-4.1.0 > /logs/$MPFR-config.log

echo $MPFR" make"
make > /logs/$MPFR-make.log
make html
echo $MPFR" check"
make check
echo $MPFR" make install"
make install > /logs/$MPFR-make_install.log
make install-html

cd /sources
rm -rf $MPFR

############ MPC ############
echo $MPC" start"

tar -xf $MPC.tar.gz
cd $MPC

echo $MPC" config"
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/mpc-1.1.0 > /logs/$MPC-config.log

echo $MPC" make"
make > /logs/$MPC-make.log
make html
echo $MPC" make install"
make install > /logs/$MPC-make_install.log
make install-html

cd /sources
rm -rf $MPC

############ ATTR ############
echo $ATTR" start"

tar -xf $ATTR.tar.gz
cd $ATTR

echo $ATTR" config"
./configure --prefix=/usr     \
            --bindir=/bin     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.4.48 > /logs/$ATTR-config.log

echo $ATTR" make"
make > /logs/$ATTR-make.log
echo $ATTR" make install"
make install > /logs/$ATTR-make_install.log

mv -v /usr/lib/libattr.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so

cd /sources
rm -rf $ATTR

############ ACL ############
echo $ACL" start"

tar -xf $ACL.tar.gz
cd $ACL

echo $ACL" config"
./configure --prefix=/usr         \
            --bindir=/bin         \
            --disable-static      \
            --libexecdir=/usr/lib \
            --docdir=/usr/share/doc/acl-2.2.53 > /logs/$ACL-config.log

echo $ACL" make"
make > /logs/$ACL-make.log
echo $ACL" make install"
make install > /logs/$ACL-make_install.log

mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so

cd /sources
rm -rf $ACL

############ LIBCAP ############
echo $LIBCAP" start"

tar -xf $LIBCAP.tar.xz
cd $LIBCAP

sed -i '/install -m.*STACAPLIBNAME/d' libcap/Makefile

echo $LIBCAP" make"
make lib=lib > /logs/$LIBCAP-make.log
echo $LIBCAP" make install"
make lib=lib PKGCONFIGDIR=/usr/lib/pkgconfig install > /logs/$LIBCAP-make_install.log

chmod -v 755 /lib/libcap.so.2.42
mv -v /lib/libpsx.a /usr/lib
rm -v /lib/libcap.so
ln -sfv ../../lib/libcap.so.2 /usr/lib/libcap.so

cd /sources
rm -rf $LIBCAP

############ SHADOW ############
echo $SHADOW" start"

tar -xf $SHADOW.tar.xz
cd $SHADOW

sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;

sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
    -e 's:/var/spool/mail:/var/mail:'                 \
    -i etc/login.defs

sed -i 's/1000/999/' etc/useradd

touch /usr/bin/passwd

echo $SHADOW" config"
./configure --sysconfdir=/etc \
            --with-group-name-max-length=32 > /logs/$SHADOW-config.log

echo $SHADOW" make"
make > /logs/$SHADOW-make.log
echo $SHADOW" make install"
make install > /logs/$SHADOW-make_install.log

pwconv
grpconv

echo "shadow passwd config"
passwd root

cd /sources
rm -rf $SHADOW

############ GCC ############
echo $GCC" start"

tar -xf $GCC.tar.xz
cd $GCC

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac

mkdir -v build
cd build

echo $GCC" config"
../configure --prefix=/usr            \
             LD=ld                    \
             --enable-languages=c,c++ \
             --disable-multilib       \
             --disable-bootstrap      \
             --with-system-zlib > /logs/$GCC-config.log

echo $GCC" make"
make > /logs/$GCC-make.log

ulimit -s 32768
chown -Rv tester .
su tester -c "PATH=$PATH make -k check"
../contrib/test_summary
echo $GCC" make install"
make install > /logs/$GCC-make_install.log
rm -rf /usr/lib/gcc/$(gcc -dumpmachine)/10.2.0/include-fixed/bits/

chown -v -R root:root \
    /usr/lib/gcc/*linux-gnu/10.2.0/include{,-fixed}

ln -sv ../usr/bin/cpp /lib

install -v -dm755 /usr/lib/bfd-plugins
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/10.2.0/liblto_plugin.so \
        /usr/lib/bfd-plugins/

echo 'int main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'

grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

grep -B4 '^ /usr/include' dummy.log

grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'

grep "/lib.*/libc.so.6 " dummy.log

grep found dummy.log

rm -v dummy.c a.out dummy.log

mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib

cd /sources
rm -rf $GCC

############ PKG_CONFIG ############
echo $PKG_CONFIG" start"

tar -xf $PKG_CONFIG.tar.gz
cd $PKG_CONFIG

echo $PKG_CONFIG" config"
./configure --prefix=/usr              \
            --with-internal-glib       \
            --disable-host-tool        \
            --docdir=/usr/share/doc/pkg-config-0.29.2 > /logs/$PKG_CONFIG-config.log

echo $PKG_CONFIG" make"
make > /logs/$PKG_CONFIG-make.log
echo $PKG_CONFIG" make install"
make install > /logs/$PKG_CONFIG-make_install.log

cd /sources
rm -rf $PKG_CONFIG

############ NCURSES ############
echo $NCURSES" start"

tar -xf $NCURSES.tar.gz
cd $NCURSES

sed -i '/LIBTOOL_INSTALL/d' c++/Makefile.in

echo $NCURSES" config"
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec > /logs/$NCURSES-config.log

echo $NCURSES" make"
make > /logs/$NCURSES-make.log
echo $NCURSES" make install"
make install > /logs/$NCURSES-make_install.log

mv -v /usr/lib/libncursesw.so.6* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so

for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done

rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so

mkdir -v       /usr/share/doc/ncurses-6.2
cp -v -R doc/* /usr/share/doc/ncurses-6.2

cd /sources
rm -rf $NCURSES

############ SED ############
echo $SED" start"

tar -xf $SED.tar.xz
cd $SED

echo $SED" config"
./configure --prefix=/usr --bindir=/bin > /logs/$SED-config.log

echo $SED" make"
make > /logs/$SED-make.log
make html

echo $SED" make install"
make install > /logs/$SED-make_install.log
install -d -m755           /usr/share/doc/sed-4.8
install -m644 doc/sed.html /usr/share/doc/sed-4.8

cd /sources
rm -rf $SED

############ PSMISC ############
echo $PSMISC" start"

tar -xf $PSMISC.tar.xz
cd $PSMISC

echo $PSMISC" config"
./configure --prefix=/usr > /logs/$PSMISC-config.log

echo $PSMISC" make"
make > /logs/$PSMISC-make.log
echo $PSMISC" make install"
make install > /logs/$PSMISC-make_install.log

mv -v /usr/bin/fuser   /bin
mv -v /usr/bin/killall /bin

cd /sources
rm -rf $PSMISC

############ GETTEXT ############
echo $GETTEXT" start"

tar -xf $GETTEXT.tar.xz
cd $GETTEXT

echo $GETTEXT" config"
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.21 > /logs/$GETTEXT-config.log

echo $GETTEXT" make"
make > /logs/$GETTEXT-make.log
echo $GETTEXT" make install"
make install > /logs/$GETTEXT-make_install.log
chmod -v 0755 /usr/lib/preloadable_libintl.so

cd /sources
rm -rf $GETTEXT

############ BISON ############
echo $BISON" start"

tar -xf $BISON.tar.xz
cd $BISON

echo $BISON" config"
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.1 > /logs/$BISON-config.log

echo $BISON" make"
make > /logs/$BISON-make.log
echo $BISON" make install"
make install > /logs/$BISON-make_install.log

cd /sources
rm -rf $BISON

############ GREP ############
echo $GREP" start"

tar -xf $GREP.tar.xz
cd $GREP

echo $GREP" config"
./configure --prefix=/usr --bindir=/bin > /logs/$GREP-config.log

echo $GREP" make"
make > /logs/$GREP-make.log
echo $GREP" make install"
make install > /logs/$GREP-make_install.log

cd /sources
rm -rf $GREP

############ BASH ############
echo $BASH" start"

tar -xf $BASH.tar.gz
cd $BASH

patch -Np1 -i ../bash-5.0-upstream_fixes-1.patch

echo $BASH" config"
./configure --prefix=/usr                    \
            --docdir=/usr/share/doc/bash-5.0 \
            --without-bash-malloc            \
            --with-installed-readline > /logs/$BASH-config.log

echo $BASH" make"
make > /logs/$BASH-make.log
echo $BASH" make install"
make install > /logs/$BASH-make_install.log

mv -vf /usr/bin/bash /bin

cd /sources
rm -rf $BASH
