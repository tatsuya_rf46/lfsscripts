# LFS Scripts
Linux From Scratch Scripts. (Version 10.0)

## Preparation
Login as root.  
`sudo su -`  
Clone this repo.  
`git clone <url>`  
`cd lfsscripts`  

## Part I
2.2 Check whether your host system meets requirments.  
`bash version-check.sh`  

## Part II
2.4 Create your partition.  
2.5 Create file system.  
`export LFS=/mnt/lfs`  
`bash -e chapter2_6-4_2.sh`  
(enter the passwd for lfs user)  
`su - lfs`  
`git clone <url>`  
`bash -e chapter4_4.sh`  

## Part III
`bash -e chapter5.sh`  
`bash -e chapter6.sh`  
`exit`  
`exit`(change to root)  
`bash -e chapter7_1-7_3.sh`  
`chroot "$LFS" /usr/bin/env -i   \`  
`    HOME=/root                  \`  
`    TERM="$TERM"                \`  
`    PS1='(lfs chroot) \u:\w\$ ' \`  
`    PATH=/bin:/usr/bin:/sbin:/usr/sbin \`  
`    /bin/bash --login +h`  
chapter7_5-7_6  
`cd /sources/lfsscripts`  
`bash -e chapter7-7_13.sh`  
`find /usr/{lib,libexec} -name \*.la -delete`  
`rm -rf /usr/share/{info,man,doc}/*`  
`bash chapter8-3_34.sh`  
`exec /bin/bash --login +h`  
`bash chapter8-35_75.sh`  