BASH=bash-5.0
BINUTILS=binutils-2.35
COREUTILS=coreutils-8.32
DIFFUTILS=diffutils-3.7
FILE=file-5.39
FINDUTILS=findutils-4.7.0
GAWK=gawk-5.1.0
GCC=gcc-10.2.0
GLIBC=glibc-2.32
GREP=grep-3.4
GZIP=gzip-1.10
LINUX=linux-5.8.3
M4=m4-1.4.18
MAKE=make-4.3
NCURSES=ncurses-6.2
PATCH=patch-2.7.6
SED=sed-4.8
TAR=tar-1.32
XZ=xz-5.2.5

export LFS=/mnt/lfs
cd $LFS/sources
if [ -d "$LFS/logs" ]; then
	echo "$LFS/logs exists"
else
	mkdir -v $LFS/logs
fi

############ M4 ############
echo $M4" start"

tar -xf $M4.tar.xz
cd $M4

sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

echo $M4" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess) > $LFS/logs/$M4-config.log

echo $M4" make"
make > $LFS/logs/$M4-make.log
echo $M4" make install"
make DESTDIR=$LFS install > $LFS/logs/$M4-make_install.log

cd $LFS/sources
rm -rf $M4

############ NCURSES ############
echo $NCURSES" start"

tar -xf $NCURSES.tar.gz
cd $NCURSES

sed -i s/mawk// configure

mkdir build
pushd build
  ../configure > $LFS/logs/$NCURSES-pushd1.log
  make -C include > $LFS/logs/$NCURSES-pushd2.log
  make -C progs tic > $LFS/logs/$NCURSES-pushd3.log
popd

echo $NCURSES" config"
./configure --prefix=/usr                \
            --host=$LFS_TGT              \
            --build=$(./config.guess)    \
            --mandir=/usr/share/man      \
            --with-manpage-format=normal \
            --with-shared                \
            --without-debug              \
            --without-ada                \
            --without-normal             \
            --enable-widec > $LFS/logs/$NCURSES-config.log

echo $NCURSES" make"
make > $LFS/logs/$NCURSES-make.log
echo $NCURSES" make install"
make DESTDIR=$LFS TIC_PATH=$(pwd)/build/progs/tic install > $LFS/logs/$NCURSES-make_install.log
echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so

mv -v $LFS/usr/lib/libncursesw.so.6* $LFS/lib

ln -sfv ../../lib/$(readlink $LFS/usr/lib/libncursesw.so) $LFS/usr/lib/libncursesw.so

cd $LFS/sources
rm -rf $NCURSES

############ BASH ############
echo $BASH" start"

tar -xf $BASH.tar.gz
cd $BASH

echo $BASH" config"
./configure --prefix=/usr                   \
            --build=$(support/config.guess) \
            --host=$LFS_TGT                 \
            --without-bash-malloc > $LFS/logs/$BASH-config.log

echo $BASH" make"
make > $LFS/logs/$BASH-make.log
echo $BASH" make install"
make DESTDIR=$LFS install > $LFS/logs/$BASH-make_install.log

mv $LFS/usr/bin/bash $LFS/bin/bash

ln -sv bash $LFS/bin/sh

cd $LFS/sources
rm -rf $BASH

############ COREUTILS ############
echo $COREUTILS" start"

tar -xf $COREUTILS.tar.xz
cd $COREUTILS

echo $COREUTILS" config"
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --enable-install-program=hostname \
            --enable-no-install-program=kill,uptime > $LFS/logs/$COREUTILS-config.log

echo $COREUTILS" make"
make > $LFS/logs/$COREUTILS-make.log
echo $COREUTILS" make install"
make DESTDIR=$LFS install > $LFS/logs/$COREUTILS-make_install.log

mv -v $LFS/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} $LFS/bin
mv -v $LFS/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm}        $LFS/bin
mv -v $LFS/usr/bin/{rmdir,stty,sync,true,uname}               $LFS/bin
mv -v $LFS/usr/bin/{head,nice,sleep,touch}                    $LFS/bin
mv -v $LFS/usr/bin/chroot                                     $LFS/usr/sbin
mkdir -pv $LFS/usr/share/man/man8
mv -v $LFS/usr/share/man/man1/chroot.1                        $LFS/usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/'                                           $LFS/usr/share/man/man8/chroot.8

cd $LFS/sources
rm -rf $COREUTILS

############ DIFFUTILS ############
echo $DIFFUTILS" start"

tar -xf $DIFFUTILS.tar.xz
cd $DIFFUTILS

echo $DIFFUTILS" config"
./configure --prefix=/usr --host=$LFS_TGT > $LFS/logs/$DIFFUTILS-config.log

echo $DIFFUTILS" make"
make > $LFS/logs/$DIFFUTILS-make.log
echo $DIFFUTILS" make install"
make DESTDIR=$LFS install > $LFS/logs/$DIFFUTILS-make_install.log

cd $LFS/sources
rm -rf $DIFFUTILS

############ FILE ############
echo $FILE" start"

tar -xf $FILE.tar.gz
cd $FILE

echo $FILE" config"
./configure --prefix=/usr --host=$LFS_TGT > $LFS/logs/$FILE-config.log

echo $FILE" make"
make > $LFS/logs/$FILE-make.log
echo $FILE" make install"
make DESTDIR=$LFS install > $LFS/logs/$FILE-make_install.log

cd $LFS/sources
rm -rf $FILE

############ FINDUTILS ############
echo $FINDUTILS" start"

tar -xf $FINDUTILS.tar.xz
cd $FINDUTILS

echo $FINDUTILS" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess) > $LFS/logs/$FINDUTILS-config.log

echo $FINDUTILS" make"
make > $LFS/logs/$FINDUTILS-make.log
echo $FINDUTILS" make install"
make DESTDIR=$LFS install > $LFS/logs/$FINDUTILS-make_install.log

mv -v $LFS/usr/bin/find $LFS/bin
sed -i 's|find:=${BINDIR}|find:=/bin|' $LFS/usr/bin/updatedb

cd $LFS/sources
rm -rf $FINDUTILS

############ GAWK ############
echo $GAWK" start"

tar -xf $GAWK.tar.xz
cd $GAWK

sed -i 's/extras//' Makefile.in

echo $GAWK" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(./config.guess) > $LFS/logs/$GAWK-config.log

echo $GAWK" make"
make > $LFS/logs/$GAWK-make.log
echo $GAWK" make install"
make DESTDIR=$LFS install > $LFS/logs/$GAWK-make_install.log

cd $LFS/sources
rm -rf $GAWK

############ GREP ############
echo $GREP" start"

tar -xf $GREP.tar.xz
cd $GREP

echo $GREP" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --bindir=/bin > $LFS/logs/$GREP-config.log

echo $GREP" make"
make > $LFS/logs/$GREP-make.log
echo $GREP" make install"
make DESTDIR=$LFS install > $LFS/logs/$GREP-make_install.log

cd $LFS/sources
rm -rf $GREP

############ GZIP ############
echo $GZIP" start"

tar -xf $GZIP.tar.xz
cd $GZIP

echo $GZIP" config"
./configure --prefix=/usr --host=$LFS_TGT > $LFS/logs/$GZIP-config.log

echo $GZIP" make"
make > $LFS/logs/$GZIP-make.log
echo $GZIP" make install"
make DESTDIR=$LFS install > $LFS/logs/$GZIP-make_install.log

mv -v $LFS/usr/bin/gzip $LFS/bin

cd $LFS/sources
rm -rf $GZIP

############ MAKE ############
echo $MAKE" start"

tar -xf $MAKE.tar.gz
cd $MAKE

echo $MAKE" config"
./configure --prefix=/usr   \
            --without-guile \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess) > $LFS/logs/$MAKE-config.log

echo $MAKE" make"
make > $LFS/logs/$MAKE-make.log
echo $MAKE" make install"
make DESTDIR=$LFS install > $LFS/logs/$MAKE-make_install.log

cd $LFS/sources
rm -rf $MAKE

############ PATCH ############
echo $PATCH" start"

tar -xf $PATCH.tar.xz
cd $PATCH

echo $PATCH" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess) > $LFS/logs/$PATCH-config.log

echo $PATCH" make"
make > $LFS/logs/$PATCH-make.log
echo $PATCH" make install"
make DESTDIR=$LFS install > $LFS/logs/$PATCH-make_install.log

cd $LFS/sources
rm -rf $PATCH

############ SED ############
echo $SED" start"

tar -xf $SED.tar.xz
cd $SED

echo $SED" config"
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --bindir=/bin > $LFS/logs/$SED-config.log

echo $SED" make"
make > $LFS/logs/$SED-make.log
echo $SED" make install"
make DESTDIR=$LFS install > $LFS/logs/$SED-make_install.log

cd $LFS/sources
rm -rf $SED

############ TAR ############
echo $TAR" start"

tar -xf $TAR.tar.xz
cd $TAR

echo $TAR" config"
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --bindir=/bin > $LFS/logs/$TAR-config.log

echo $TAR" make"
make > $LFS/logs/$TAR-make.log
echo $TAR" make install"
make DESTDIR=$LFS install > $LFS/logs/$TAR-make_install.log

cd $LFS/sources
rm -rf $TAR

############ XZ ############
echo $XZ" start"

tar -xf $XZ.tar.xz
cd $XZ

echo $XZ" config"
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --disable-static                  \
            --docdir=/usr/share/doc/xz-5.2.5 > $LFS/logs/$XZ-config.log

echo $XZ" make"
make > $LFS/logs/$XZ-make.log
echo $XZ" make install"
make DESTDIR=$LFS install > $LFS/logs/$XZ-make_install.log

mv -v $LFS/usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat}  $LFS/bin
mv -v $LFS/usr/lib/liblzma.so.*                       $LFS/lib
ln -svf ../../lib/$(readlink $LFS/usr/lib/liblzma.so) $LFS/usr/lib/liblzma.so

cd $LFS/sources
rm -rf $XZ

############ BINUTILS ############
echo $BINUTILS" start"

tar -xf $BINUTILS.tar.xz
cd $BINUTILS

mkdir -v build
cd build

echo $BINUTILS" config"
../configure                   \
    --prefix=/usr              \
    --build=$(../config.guess) \
    --host=$LFS_TGT            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror           \
    --enable-64-bit-bfd > $LFS/logs/$BINUTILS-config.log

echo $BINUTILS" make"
make > $LFS/logs/$BINUTILS-make.log
echo $BINUTILS" make install"
make DESTDIR=$LFS install > $LFS/logs/$BINUTILS-make_install.log

cd $LFS/sources
rm -rf $BINUTILS

############ GCC ############
echo $GCC" start"

tar -xf $GCC.tar.xz
cd $GCC

tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.0.tar.xz
mv -v gmp-6.2.0 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
  ;;
esac

mkdir -v build
cd       build

mkdir -pv $LFS_TGT/libgcc
ln -s ../../../libgcc/gthr-posix.h $LFS_TGT/libgcc/gthr-default.h

echo $GCC" config"
../configure                                       \
    --build=$(../config.guess)                     \
    --host=$LFS_TGT                                \
    --prefix=/usr                                  \
    CC_FOR_TARGET=$LFS_TGT-gcc                     \
    --with-build-sysroot=$LFS                      \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++ > $LFS/logs/$GCC-config.log

echo $GCC" make"
make > $LFS/logs/$GCC-make.log
echo $GCC" make install"
make DESTDIR=$LFS install > $LFS/logs/$GCC-make_install.log

ln -sv gcc $LFS/usr/bin/cc

cd $LFS/sources
rm -rf $GCC

############ END ############
rm -rf $LFS/logs/*
