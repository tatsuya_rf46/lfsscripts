ROOT=/dev/nvme0n1p5
HOME=/dev/nvme0n1p3

export LFS=/mnt/lfs
mkdir -pv $LFS
mount -v -t ext4 $ROOT $LFS
mkdir -v $LFS/home
mount -v -t ext4 $HOME $LFS/home

mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources

mkdir -v $LFS/logs
chmod -v a+wt $LFS/logs

wget --input-file=wget-list --continue --directory-prefix=$LFS/sources

cp md5sums $LFS/sources

pushd $LFS/sources
	md5sum -c md5sums
popd

cd ..
cp -r lfsscripts $LFS/sources
cd lfsscripts

mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var}
case $(uname -m) in
  x86_64) mkdir -pv $LFS/lib64 ;;
esac

mkdir -pv $LFS/tools

groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs

passwd lfs

chown -v lfs $LFS/{usr,lib,var,etc,bin,sbin,tools}
case $(uname -m) in
  x86_64) chown -v lfs $LFS/lib64 ;;
esac

chown -v lfs $LFS/sources
chown -v lfs $LFS/logs

[ ! -e /etc/bash.bashrc ] || mv -v /etc/bash.bashrc /etc/bash.bashrc.NOUSE
