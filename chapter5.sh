BINUTILS=binutils-2.35
GCC=gcc-10.2.0
GLIBC=glibc-2.32
LINUX=linux-5.8.3

export LFS=/mnt/lfs
cd $LFS/sources
if [ -d "$LFS/logs" ]; then
	echo "$LFS/logs exists"
else
	mkdir -v $LFS/logs
fi

############ BINUTILS ############
echo $BINUTILS" start"

tar -xf $BINUTILS.tar.xz
cd $BINUTILS

mkdir -v build
cd build

echo $BINUTILS" config"
../configure --prefix=$LFS/tools       \
             --with-sysroot=$LFS        \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror > $LFS/logs/$BINUTILS-config.log

echo $BINUTILS" make"
make > $LFS/logs/$BINUTILS-make.log
echo $BINUTILS" make install"
make install > $LFS/logs/$BINUTILS-make_install.log

cd $LFS/sources
rm -rf $BINUTILS

############ GCC ############
echo $GCC" start"

tar -xf $GCC.tar.xz
cd $GCC

tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.0.tar.xz
mv -v gmp-6.2.0 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac

mkdir -v build
cd build

echo $GCC" config"
../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=$LFS/tools                            \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++ > $LFS/logs/$GCC-config.log

echo $GCC" make"
make > $LFS/logs/$GCC-make.log
echo $GCC" make install"
make install > $LFS/logs/$GCC-make_install.log

cd ..
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h

cd $LFS/sources
rm -rf $GCC

############ LINUX ############
echo $LINUX" start"

tar -xf $LINUX.tar.xz
cd $LINUX

make mrproper

make headers
find usr/include -name '.*' -delete
rm usr/include/Makefile
cp -rv usr/include $LFS/usr

cd $LFS/sources
rm -rf $LINUX

############ GLIBC ############
echo $GLIBC" start"

tar -xf $GLIBC.tar.xz
cd $GLIBC

case $(uname -m) in
    i?86)   ln -sfv ld-linux.so.2 $LFS/lib/ld-lsb.so.3
    ;;
    x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3
    ;;
esac

patch -Np1 -i ../glibc-2.32-fhs-1.patch

mkdir -v build
cd build

echo $GLIBC" config"
../configure                             \
      --prefix=/usr                      \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=$LFS/usr/include    \
      libc_cv_slibdir=/lib > $LFS/logs/$GLIBC-config.log

echo $GLIBC" make"
make > $LFS/logs/$GLIBC-make.log
echo $GLIBC" make install"
make DESTDIR=$LFS install > $LFS/logs/$GLIBC-make_install.log

$LFS/tools/libexec/gcc/$LFS_TGT/10.2.0/install-tools/mkheaders

cd $LFS/sources
rm -rf $GLIBC

############ LIBSTDC++ ############
echo "libstdc++ start"

tar -xf $GCC.tar.xz
cd $GCC

mkdir -v build
cd build

echo "libstdc++ config"
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --build=$(../config.guess)      \
    --prefix=/usr                   \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/10.2.0 > $LFS/logs/libstdc++-config.log

echo "libstdc++ make"
make > $LFS/logs/libstdc++-make.log
echo "libstdc++ make install"
make DESTDIR=$LFS install > $LFS/logs/libstdc++-make_install.log

cd $LFS/sources
rm -rf $GCC
