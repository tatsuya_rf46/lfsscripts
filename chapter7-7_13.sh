#acl-2.2.53
#attr-2.4.48
#autoconf-2.69
#automake-1.16.2
#bash-5.0
#bash-5.0-upstream_fixes-1.patch
#bc-3.1.5
#binutils-2.35
BISON=bison-3.7.1
#bzip2-1.0.8-install_docs-1.patch
#bzip2-1.0.8
#check-0.15.2
#coreutils-8.32-i18n-1.patch
#coreutils-8.32
#dbus-1.12.20
#dejagnu-1.6.2
#diffutils-3.7
#e2fsprogs-1.45.6
#elfutils-0.180
#eudev-3.2.9
#expat-2.2.9
#expect5.45.4
#file-5.39
#findutils-4.7.0
#flex-2.6.4
#gawk-5.1.0
GCC=gcc-10.2.0
#gdbm-1.18.1
GETTEXT=gettext-0.21
#glibc-2.32-fhs-1.patch
#glibc-2.32
#gmp-6.2.0
#gperf-3.1
#grep-3.4
#groff-1.22.4
#grub-2.04
#gzip-1.10
#iana-etc-20200821
#inetutils-1.9.4
#intltool-0.51.0
#iproute2-5.8.0
#kbd-2.3.0-backspace-1.patch
#kbd-2.3.0
#kmod-27
#less-551
#lfs-bootscripts-20200818
#libcap-2.42
#libffi-3.3
#libpipeline-1.5.3
#libtool-2.4.6
#linux-5.8.3
#m4-1.4.18
#make-4.3
#man-db-2.9.3
#man-pages-5.08
#meson-0.55.0
#mpc-1.1.0
#mpfr-4.1.0
#ncurses-6.2
#ninja-1.10.0
#openssl-1.1.1g
#patch-2.7.6
PERL=perl-5.32.0
#pkg-config-0.29.2
#procps-ng-3.3.16
#psmisc-23.3
#python-3.8.5-docs-html
PYTHON=Python-3.8.5
#readline-8.0
#sed-4.8
#shadow-4.8.1
#sysklogd-1.5.1
#systemd-246
#systemd-man-pages-246
#sysvinit-2.97-consolidated-1.patch
#sysvinit-2.97
#tar-1.32
#tcl8.6.10-html
#tcl8.6.10-src
TEXINFO=texinfo-6.7
#tzdata2020a
#udev-lfs-20171102
UTIL_LINUX=util-linux-2.36
#vim-8.2.1361
#XML-Parser-2.46
#xz-5.2.5
#zlib-1.2.11
#zstd-1.4.5

cd /sources

############ LIBSTDXX ############
echo "LIBSTDXX start"

tar -xf $GCC.tar.xz
cd $GCC

ln -s gthr-posix.h libgcc/gthr-default.h

mkdir -v build
cd build

echo "LIBSTDXX config"
../libstdc++-v3/configure            \
    CXXFLAGS="-g -O2 -D_GNU_SOURCE"  \
    --prefix=/usr                    \
    --disable-multilib               \
    --disable-nls                    \
    --host=$(uname -m)-lfs-linux-gnu \
    --disable-libstdcxx-pch > /logs/$GCC-config.log

echo $GCC" make"
make > /logs/$GCC-make.log
echo $GCC" make install"
make install > /logs/$GCC-make_install.log

cd /sources
rm -rf $GCC

############ GETTEXT ############
echo $GETTEXT" start"

tar -xf $GETTEXT.tar.xz
cd $GETTEXT

echo $GETTEXT" config"
./configure --disable-shared > /logs/$GETTEXT-config.log

echo $GETTEXT" make"
make > /logs/$GETTEXT-make.log
echo $GETTEXT" make install"
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin > /logs/$GETTEXT-make_install.log

cd /sources
rm -rf $GETTEXT

############ BISON ############
echo $BISON" start"

tar -xf $BISON.tar.xz
cd $BISON

echo $BISON" config"
./configure --prefix=/usr \
            --docdir=/usr/share/doc/bison-3.7.1 > /logs/$BISON-config.log

echo $BISON" make"
make > /logs/$BISON-make.log
echo $BISON" make install"
make install > /logs/$BISON-make_install.log

cd /sources
rm -rf $BISON

############ PERL ############
echo $PERL" start"

tar -xf $PERL.tar.xz
cd $PERL

echo $PERL" config"
sh Configure -des                                        \
             -Dprefix=/usr                               \
             -Dvendorprefix=/usr                         \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl     \
             -Darchlib=/usr/lib/perl5/5.32/core_perl     \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl     \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl    \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl > /logs/$PERL-config.log

echo $PERL" make"
make > /logs/$PERL-make.log
echo $PERL" make install"
make install > /logs/$PERL-make_install.log

cd /sources
rm -rf $PERL

############ PYTHON ############
echo $PYTHON" start"

tar -xf $PYTHON.tar.xz
cd $PYTHON

echo $PYTHON" config"
./configure --prefix=/usr   \
            --enable-shared \
            --without-ensurepip > /logs/$PYTHON-config.log

echo $PYTHON" make"
make > /logs/$PYTHON-make.log
echo $PYTHON" make install"
make install > /logs/$PYTHON-make_install.log

cd /sources
rm -rf $PYTHON

############ TEXINFO ############
echo $TEXINFO" start"

tar -xf $TEXINFO.tar.xz
cd $TEXINFO

echo $TEXINFO" config"
./configure --prefix=/usr > /logs/$TEXINFO-config.log

echo $TEXINFO" make"
make > /logs/$TEXINFO-make.log
echo $TEXINFO" make install"
make install > /logs/$TEXINFO-make_install.log

cd /sources
rm -rf $TEXINFO

############ UTIL_LINUX ############
echo $UTIL_LINUX" start"

tar -xf $UTIL_LINUX.tar.xz
cd $UTIL_LINUX

mkdir -pv /var/lib/hwclock

echo $UTIL_LINUX" config"
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime    \
            --docdir=/usr/share/doc/util-linux-2.36 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python > /logs/$UTIL_LINUX-config.log

echo $UTIL_LINUX" make"
make > /logs/$UTIL_LINUX-make.log
echo $UTIL_LINUX" make install"
make install > /logs/$UTIL_LINUX-make_install.log

cd /sources
rm -rf $UTIL_LINUX
