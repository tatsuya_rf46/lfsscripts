ACL=acl-2.2.53
ATTR=attr-2.4.48
AUTOCONF=autoconf-2.69
AUTOMAKE=automake-1.16.2
BASH=bash-5.0
#bash-5.0-upstream_fixes-1.patch
BC=bc-3.1.5
BINUTILS=binutils-2.35
BISON=bison-3.7.1
#bzip2-1.0.8-install_docs-1.patch
BZIP2=bzip2-1.0.8
CHECK=check-0.15.2
#coreutils-8.32-i18n-1.patch
COREUTILS=coreutils-8.32
#dbus-1.12.20
DEJAGNU=dejagnu-1.6.2
DIFFUTILS=diffutils-3.7
E2FSPROGS=e2fsprogs-1.45.6
LIBELF=elfutils-0.180
EUDEV=eudev-3.2.9
EXPAT=expat-2.2.9
EXPECT=expect5.45.4
FILE=file-5.39
FINDUTILS=findutils-4.7.0
FLEX=flex-2.6.4
GAWK=gawk-5.1.0
GCC=gcc-10.2.0
GDBM=gdbm-1.18.1
GETTEXT=gettext-0.21
#glibc-2.32-fhs-1.patch
GLIBC=glibc-2.32
GMP=gmp-6.2.0
GPERF=gperf-3.1
GREP=grep-3.4
GROFF=groff-1.22.4
GRUB=grub-2.04
GZIP=gzip-1.10
IANA=iana-etc-20200821
INETUTILS=inetutils-1.9.4
INTLTOOL=intltool-0.51.0
IPROUTE=iproute2-5.8.0
#kbd-2.3.0-backspace-1.patch
KBD=kbd-2.3.0
KMOD=kmod-27
LESS=less-551
#lfs-bootscripts-20200818
LIBCAP=libcap-2.42
LIBFFI=libffi-3.3
LIBPIPELINE=libpipeline-1.5.3
LIBTOOL=libtool-2.4.6
#linux-5.8.3
M4=m4-1.4.18
MAKE=make-4.3
MAN_DB=man-db-2.9.3
MAN_PAGES=man-pages-5.08
MESON=meson-0.55.0
MPC=mpc-1.1.0
MPFR=mpfr-4.1.0
NCURSES=ncurses-6.2
NINJA=ninja-1.10.0
OPENSSL=openssl-1.1.1g
PATCH=patch-2.7.6
PERL=perl-5.32.0
PKG_CONFIG=pkg-config-0.29.2
PROCPS=procps-ng-3.3.16
PSMISC=psmisc-23.3
#python-3.8.5-docs-html
PYTHON=Python-3.8.5
READLINE=readline-8.0
SED=sed-4.8
SHADOW=shadow-4.8.1
SYSKLOGD=sysklogd-1.5.1
#systemd-246
#systemd-man-pages-246
#sysvinit-2.97-consolidated-1.patch
SYSVINIT=sysvinit-2.97
TAR=tar-1.32
#tcl8.6.10-html
TCL=tcl8.6.10-src
TEXINFO=texinfo-6.7
#tzdata2020a
#udev-lfs-20171102
UTIL_LINUX=util-linux-2.36
VIM=vim-8.2.1361
XML=XML-Parser-2.46
XZ=xz-5.2.5
ZLIB=zlib-1.2.11
ZSTD=zstd-1.4.5

cd $LFS/sources

############ LIBTOOL ############
echo $LIBTOOL" start"

tar -xf $LIBTOOL.tar.xz
cd $LIBTOOL

echo $LIBTOOL" config"
./configure --prefix=/usr > /logs/$LIBTOOL-config.log

echo $LIBTOOL" make"
make > /logs/$LIBTOOL-make.log
echo $LIBTOOL" make install"
make install > /logs/$LIBTOOL-make_install.log

cd /sources
rm -rf $LIBTOOL

############ GDBM ############
echo $GDBM" start"

tar -xf $GDBM.tar.gz
cd $GDBM

sed -r -i '/^char.*parseopt_program_(doc|args)/d' src/parseopt.c

echo $GDBM" config"
./configure --prefix=/usr    \
            --disable-static \
            --enable-libgdbm-compat > /logs/$GDBM-config.log

echo $GDBM" make"
make > /logs/$GDBM-make.log
echo $GDBM" make install"
make install > /logs/$GDBM-make_install.log

cd /sources
rm -rf $GDBM

############ GPERF ############
echo $GPERF" start"

tar -xf $GPERF.tar.gz
cd $GPERF

echo $GPERF" config"
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1 > /logs/$GPERF-config.log

echo $GPERF" make"
make > /logs/$GPERF-make.log
echo $GPERF" make install"
make install > /logs/$GPERF-make_install.log

cd /sources
rm -rf $GPERF

############ EXPAT ############
echo $EXPAT" start"

tar -xf $EXPAT.tar.xz
cd $EXPAT

echo $EXPAT" config"
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/expat-2.2.9 > /logs/$EXPAT-config.log

echo $EXPAT" make"
make > /logs/$EXPAT-make.log
echo $EXPAT" make install"
make install > /logs/$EXPAT-make_install.log

install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.9

cd /sources
rm -rf $EXPAT

############ INETUTILS ############
echo $INETUTILS" start"

tar -xf $INETUTILS.tar.xz
cd $INETUTILS

echo $INETUTILS" config"
./configure --prefix=/usr        \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers > /logs/$INETUTILS-config.log

echo $INETUTILS" make"
make > /logs/$INETUTILS-make.log
echo $INETUTILS" make install"
make install > /logs/$INETUTILS-make_install.log

mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin

cd /sources
rm -rf $INETUTILS

############ PERL ############
echo $PERL" start"

tar -xf $PERL.tar.xz
cd $PERL

export BUILD_ZLIB=False
export BUILD_BZIP2=0

echo $PERL" config"
sh Configure -des                                         \
             -Dprefix=/usr                                \
             -Dvendorprefix=/usr                          \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl      \
             -Darchlib=/usr/lib/perl5/5.32/core_perl      \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl      \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl     \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl  \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl \
             -Dman1dir=/usr/share/man/man1                \
             -Dman3dir=/usr/share/man/man3                \
             -Dpager="/usr/bin/less -isR"                 \
             -Duseshrplib                                 \
             -Dusethreads > /logs/$PERL-config.log

echo $PERL" make"
make > /logs/$PERL-make.log
echo $PERL" make install"
make install > /logs/$PERL-make_install.log

unset BUILD_ZLIB BUILD_BZIP2

cd /sources
rm -rf $PERL

############ XML ############
echo $XML" start"

tar -xf $XML.tar.gz
cd $XML

echo $XML" config"
perl Makefile.PL > /logs/$XML-config.log

echo $XML" make"
make > /logs/$XML-make.log
echo $XML" make install"
make install > /logs/$XML-make_install.log

cd /sources
rm -rf $XML

############ INTLTOOL ############
echo $INTLTOOL" start"

tar -xf $INTLTOOL.tar.gz
cd $INTLTOOL

sed -i 's:\\\${:\\\$\\{:' intltool-update.in

echo $INTLTOOL" config"
./configure --prefix=/usr > /logs/$INTLTOOL-config.log

echo $INTLTOOL" make"
make > /logs/$INTLTOOL-make.log
echo $INTLTOOL" make install"
make install > /logs/$INTLTOOL-make_install.log

install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO

cd /sources
rm -rf $INTLTOOL

############ AUTOCONF ############
echo $AUTOCONF" start"

tar -xf $AUTOCONF.tar.xz
cd $AUTOCONF

sed -i '361 s/{/\\{/' bin/autoscan.in

echo $AUTOCONF" config"
./configure --prefix=/usr > /logs/$AUTOCONF-config.log

echo $AUTOCONF" make"
make > /logs/$AUTOCONF-make.log
echo $AUTOCONF" make install"
make install > /logs/$AUTOCONF-make_install.log

cd /sources
rm -rf $AUTOCONF

############ AUTOMAKE ############
echo $AUTOMAKE" start"

tar -xf $AUTOMAKE.tar.xz
cd $AUTOMAKE

sed -i "s/''/etags/" t/tags-lisp-space.sh

echo $AUTOMAKE" config"
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.2 > /logs/$AUTOMAKE-config.log

echo $AUTOMAKE" make"
make > /logs/$AUTOMAKE-make.log
echo $AUTOMAKE" make install"
make install > /logs/$AUTOMAKE-make_install.log

cd /sources
rm -rf $AUTOMAKE

############ KMOD ############
echo $KMOD" start"

tar -xf $KMOD.tar.xz
cd $KMOD

echo $KMOD" config"
./configure --prefix=/usr          \
            --bindir=/bin          \
            --sysconfdir=/etc      \
            --with-rootlibdir=/lib \
            --with-xz              \
            --with-zlib > /logs/$KMOD-config.log

echo $KMOD" make"
make > /logs/$KMOD-make.log
echo $KMOD" make install"
make install > /logs/$KMOD-make_install.log

for target in depmod insmod lsmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /sbin/$target
done

ln -sfv kmod /bin/lsmod

cd /sources
rm -rf $KMOD

############ LIBELF ############
echo $LIBELF" start"

tar -xf $LIBELF.tar.bz2
cd $LIBELF

echo $LIBELF" config"
./configure --prefix=/usr --disable-debuginfod --libdir=/lib > /logs/$LIBELF-config.log

echo $LIBELF" make"
make > /logs/$LIBELF-make.log
echo $LIBELF" make install"
make -C libelf install > /logs/$LIBELF-make_install.log
install -vm644 config/libelf.pc /usr/lib/pkgconfig
rm /lib/libelf.a

cd /sources
rm -rf $LIBELF

############ LIBFFI ############
echo $LIBFFI" start"

tar -xf $LIBFFI.tar.gz
cd $LIBFFI

echo $LIBFFI" config"
./configure --prefix=/usr --disable-static --with-gcc-arch=native > /logs/$LIBFFI-config.log

echo $LIBFFI" make"
make > /logs/$LIBFFI-make.log
echo $LIBFFI" make install"
make install > /logs/$LIBFFI-make_install.log

cd /sources
rm -rf $LIBFFI

############ OPENSSL ############
echo $OPENSSL" start"

tar -xf $OPENSSL.tar.gz
cd $OPENSSL

echo $OPENSSL" config"
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic > /logs/$OPENSSL-config.log

echo $OPENSSL" make"
make > /logs/$OPENSSL-make.log

sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile

echo $OPENSSL" make install"
make MANSUFFIX=ssl install > /logs/$OPENSSL-make_install.log

mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1g
cp -vfr doc/* /usr/share/doc/openssl-1.1.1g

cd /sources
rm -rf $OPENSSL

############ PYTHON ############
echo $PYTHON" start"

tar -xf $PYTHON.tar.xz
cd $PYTHON

echo $PYTHON" config"
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes > /logs/$PYTHON-config.log

echo $PYTHON" make"
make > /logs/$PYTHON-make.log
echo $PYTHON" make install"
make install > /logs/$PYTHON-make_install.log

chmod -v 755 /usr/lib/libpython3.8.so
chmod -v 755 /usr/lib/libpython3.so
ln -sfv pip3.8 /usr/bin/pip3

install -v -dm755 /usr/share/doc/python-3.8.5/html

tar --strip-components=1  \
    --no-same-owner       \
    --no-same-permissions \
    -C /usr/share/doc/python-3.8.5/html \
    -xvf ../python-3.8.5-docs-html.tar.bz2

cd /sources
rm -rf $PYTHON

############ NINJA ############
echo $NINJA" start"

tar -xf $NINJA.tar.gz
cd $NINJA

export NINJAJOBS=4

echo $NINJA" config"
sed -i '/int Guess/a \
  int   j = 0;\
  char* jobs = getenv( "NINJAJOBS" );\
  if ( jobs != NULL ) j = atoi( jobs );\
  if ( j > 0 ) return j;\
' src/ninja.cc > /logs/$NINJA-config.log

echo $NINJA" make"
python3 configure.py --bootstrap > /logs/$NINJA-make.log

install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja

cd /sources
rm -rf $NINJA

############ MESON ############
echo $MESON" start"

tar -xf $MESON.tar.gz
cd $MESON

echo $MESON" make"
python3 setup.py build > /logs/$MESON-make.log
echo $MESON" make install"
python3 setup.py install --root=dest > /logs/$MESON-make_install.log
cp -rv dest/* /

cd /sources
rm -rf $MESON

############ COREUTILS ############
echo $COREUTILS" start"

tar -xf $COREUTILS.tar.xz
cd $COREUTILS

patch -Np1 -i ../coreutils-8.32-i18n-1.patch
sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk

autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime

echo $COREUTILS" make"
make > /logs/$COREUTILS-make.log

echo $COREUTILS" test"
make NON_ROOT_USERNAME=tester check-root

echo "dummy:x:102:tester" >> /etc/group
chown -Rv tester .
su tester -c "PATH=$PATH make RUN_EXPENSIVE_TESTS=yes check" > /logs/$COREUTILS-check.log

sed -i '/dummy/d' /etc/group

echo $COREUTILS" make install"
make install > /logs/$COREUTILS-make_install.log

mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8

mv -v /usr/bin/{head,nice,sleep,touch} /bin

cd /sources
rm -rf $COREUTILS

############ CHECK ############
echo $CHECK" start"

tar -xf $CHECK.tar.gz
cd $CHECK

echo $CHECK" config"
./configure --prefix=/usr --disable-static > /logs/$CHECK-config.log

echo $CHECK" make"
make > /logs/$CHECK-make.log
echo $CHECK" make install"
make docdir=/usr/share/doc/check-0.15.2 install > /logs/$CHECK-make_install.log

cd /sources
rm -rf $CHECK

############ DIFFUTILS ############
echo $DIFFUTILS" start"

tar -xf $DIFFUTILS.tar.xz
cd $DIFFUTILS

echo $DIFFUTILS" config"
./configure --prefix=/usr > /logs/$DIFFUTILS-config.log

echo $DIFFUTILS" make"
make > /logs/$DIFFUTILS-make.log
echo $DIFFUTILS" make install"
make install > /logs/$DIFFUTILS-make_install.log

cd /sources
rm -rf $DIFFUTILS

############ GAWK ############
echo $GAWK" start"

tar -xf $GAWK.tar.xz
cd $GAWK

sed -i 's/extras//' Makefile.in

echo $GAWK" config"
./configure --prefix=/usr > /logs/$GAWK-config.log

echo $GAWK" make"
make > /logs/$GAWK-make.log
echo $GAWK" make install"
make install > /logs/$GAWK-make_install.log

mkdir -v /usr/share/doc/gawk-5.1.0
cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-5.1.0

cd /sources
rm -rf $GAWK

############ FINDUTILS ############
echo $FINDUTILS" start"

tar -xf $FINDUTILS.tar.xz
cd $FINDUTILS

echo $FINDUTILS" config"
./configure --prefix=/usr --localstatedir=/var/lib/locate > /logs/$FINDUTILS-config.log

echo $FINDUTILS" make"
make > /logs/$FINDUTILS-make.log
echo $FINDUTILS" make install"
make install > /logs/$FINDUTILS-make_install.log

mv -v /usr/bin/find /bin
sed -i 's|find:=${BINDIR}|find:=/bin|' /usr/bin/updatedb

cd /sources
rm -rf $FINDUTILS

############ GROFF ############
echo $GROFF" start"

tar -xf $GROFF.tar.gz
cd $GROFF

echo $GROFF" config"
PAGE=A4 ./configure --prefix=/usr > /logs/$GROFF-config.log

echo $GROFF" make"
make -j1 > /logs/$GROFF-make.log
echo $GROFF" make install"
make install > /logs/$GROFF-make_install.log

cd /sources
rm -rf $GROFF

############ GRUB ############
echo $GRUB" start"

tar -xf $GRUB.tar.xz
cd $GRUB

echo $GRUB" config"
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-efiemu       \
            --disable-werror > /logs/$GRUB-config.log

echo $GRUB" make"
make > /logs/$GRUB-make.log
echo $GRUB" make install"
make install > /logs/$GRUB-make_install.log

mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions

cd /sources
rm -rf $GRUB

############ LESS ############
echo $LESS" start"

tar -xf $LESS.tar.gz
cd $LESS

echo $LESS" config"
./configure --prefix=/usr --sysconfdir=/etc > /logs/$LESS-config.log

echo $LESS" make"
make > /logs/$LESS-make.log
echo $LESS" make install"
make install > /logs/$LESS-make_install.log

cd /sources
rm -rf $LESS

############ GZIP ############
echo $GZIP" start"

tar -xf $GZIP.tar.xz
cd $GZIP

echo $GZIP" config"
./configure --prefix=/usr > /logs/$GZIP-config.log

echo $GZIP" make"
make > /logs/$GZIP-make.log
echo $GZIP" make install"
make install > /logs/$GZIP-make_install.log

mv -v /usr/bin/gzip /bin

cd /sources
rm -rf $GZIP

############ IPROUTE ############
echo $IPROUTE" start"

tar -xf $IPROUTE.tar.xz
cd $IPROUTE

sed -i /ARPD/d Makefile
rm -fv man/man8/arpd.8

sed -i 's/.m_ipt.o//' tc/Makefile

echo $IPROUTE" make"
make > /logs/$IPROUTE-make.log
echo $IPROUTE" make install"
make DOCDIR=/usr/share/doc/iproute2-5.8.0 install > /logs/$IPROUTE-make_install.log

cd /sources
rm -rf $IPROUTE

############ KBD ############
echo $KBD" start"

tar -xf $KBD.tar.xz
cd $KBD

patch -Np1 -i ../kbd-2.3.0-backspace-1.patch

sed -i '/RESIZECONS_PROGS=/s/yes/no/' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in

echo $KBD" config"
./configure --prefix=/usr --disable-vlock > /logs/$KBD-config.log

echo $KBD" make"
make > /logs/$KBD-make.log
echo $KBD" make install"
make install > /logs/$KBD-make_install.log

rm -v /usr/lib/libtswrap.{a,la,so*}

mkdir -v            /usr/share/doc/kbd-2.3.0
cp -R -v docs/doc/* /usr/share/doc/kbd-2.3.0

cd /sources
rm -rf $KBD

############ LIBPIPELINE ############
echo $LIBPIPELINE" start"

tar -xf $LIBPIPELINE.tar.gz
cd $LIBPIPELINE

echo $LIBPIPELINE" config"
./configure --prefix=/usr > /logs/$LIBPIPELINE-config.log

echo $LIBPIPELINE" make"
make > /logs/$LIBPIPELINE-make.log
echo $LIBPIPELINE" make install"
make install > /logs/$LIBPIPELINE-make_install.log

cd /sources
rm -rf $LIBPIPELINE

############ MAKE ############
echo $MAKE" start"

tar -xf $MAKE.tar.gz
cd $MAKE

echo $MAKE" config"
./configure --prefix=/usr > /logs/$MAKE-config.log

echo $MAKE" make"
make > /logs/$MAKE-make.log
echo $MAKE" make install"
make install > /logs/$MAKE-make_install.log

cd /sources
rm -rf $MAKE

############ PATCH ############
echo $PATCH" start"

tar -xf $PATCH.tar.xz
cd $PATCH

echo $PATCH" config"
./configure --prefix=/usr > /logs/$PATCH-config.log

echo $PATCH" make"
make > /logs/$PATCH-make.log
echo $PATCH" make install"
make install > /logs/$PATCH-make_install.log

cd /sources
rm -rf $PATCH

############ MAN_DB ############
echo $MAN_DB" start"

tar -xf $MAN_DB.tar.xz
cd $MAN_DB

echo $MAN_DB" config"
./configure --prefix=/usr                        \
            --docdir=/usr/share/doc/man-db-2.9.3 \
            --sysconfdir=/etc                    \
            --disable-setuid                     \
            --enable-cache-owner=bin             \
            --with-browser=/usr/bin/lynx         \
            --with-vgrind=/usr/bin/vgrind        \
            --with-grap=/usr/bin/grap            \
            --with-systemdtmpfilesdir=           \
            --with-systemdsystemunitdir= > /logs/$MAN_DB-config.log

echo $MAN_DB" make"
make > /logs/$MAN_DB-make.log
echo $MAN_DB" make install"
make install > /logs/$MAN_DB-make_install.log

cd /sources
rm -rf $MAN_DB

############ TAR ############
echo $TAR" start"

tar -xf $TAR.tar.xz
cd $TAR

echo $TAR" config"
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr \
            --bindir=/bin > /logs/$TAR-config.log

echo $TAR" make"
make > /logs/$TAR-make.log
echo $TAR" make install"
make install > /logs/$TAR-make_install.log

make -C doc install-html docdir=/usr/share/doc/tar-1.32

cd /sources
rm -rf $TAR

############ TEXINFO ############
echo $TEXINFO" start"

tar -xf $TEXINFO.tar.xz
cd $TEXINFO

echo $TEXINFO" config"
./configure --prefix=/usr --disable-static > /logs/$TEXINFO-config.log

echo $TEXINFO" make"
make > /logs/$TEXINFO-make.log
echo $TEXINFO" make install"
make install > /logs/$TEXINFO-make_install.log

make TEXMF=/usr/share/texmf install-tex

pushd /usr/share/info
  rm -v dir
  for f in *
    do install-info $f dir 2>/dev/null
  done
popd

cd /sources
rm -rf $TEXINFO

############ VIM ############
echo $VIM" start"

tar -xf $VIM.tar.gz
cd $VIM

echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h

echo $VIM" config"
./configure --prefix=/usr > /logs/$VIM-config.log

echo $VIM" make"
make > /logs/$VIM-make.log
echo $VIM" make install"
make install > /logs/$VIM-make_install.log

ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done

ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.1361

cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc

" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1

set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF

cd /sources
rm -rf $VIM

############ EUDEV ############
echo $EUDEV" start"

tar -xf $EUDEV.tar.gz
cd $EUDEV

echo $EUDEV" config"
./configure --prefix=/usr           \
            --bindir=/sbin          \
            --sbindir=/sbin         \
            --libdir=/usr/lib       \
            --sysconfdir=/etc       \
            --libexecdir=/lib       \
            --with-rootprefix=      \
            --with-rootlibdir=/lib  \
            --enable-manpages       \
            --disable-static > /logs/$EUDEV-config.log

echo $EUDEV" make"
make > /logs/$EUDEV-make.log

mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d

echo $EUDEV" make install"
make install > /logs/$EUDEV-make_install.log

tar -xvf ../udev-lfs-20171102.tar.xz
make -f udev-lfs-20171102/Makefile.lfs install

udevadm hwdb --update

cd /sources
rm -rf $EUDEV

############ PROCPS ############
echo $PROCPS" start"

tar -xf $PROCPS.tar.xz
cd $PROCPS

echo $PROCPS" config"
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --libdir=/usr/lib                        \
            --docdir=/usr/share/doc/procps-ng-3.3.16 \
            --disable-static                         \
            --disable-kill > /logs/$PROCPS-config.log

echo $PROCPS" make"
make > /logs/$PROCPS-make.log
echo $PROCPS" make install"
make install > /logs/$PROCPS-make_install.log

mv -v /usr/lib/libprocps.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so

cd /sources
rm -rf $PROCPS

############ UTIL_LINUX ############
echo $UTIL_LINUX" start"

tar -xf $UTIL_LINUX.tar.xz
cd $UTIL_LINUX

mkdir -pv /var/lib/hwclock

echo $UTIL_LINUX" config"
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --docdir=/usr/share/doc/util-linux-2.36 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir > /logs/$UTIL_LINUX-config.log

echo $UTIL_LINUX" make"
make > /logs/$UTIL_LINUX-make.log
echo $UTIL_LINUX" make install"
make install > /logs/$UTIL_LINUX-make_install.log

cd /sources
rm -rf $UTIL_LINUX

############ E2FSPROGS ############
echo $E2FSPROGS" start"

tar -xf $E2FSPROGS.tar.gz
cd $E2FSPROGS

mkdir -v build
cd build

echo $E2FSPROGS" config"
../configure --prefix=/usr           \
             --bindir=/bin           \
             --with-root-prefix=""   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck > /logs/$E2FSPROGS-config.log

echo $E2FSPROGS" make"
make > /logs/$E2FSPROGS-make.log
echo $E2FSPROGS" make install"
make install > /logs/$E2FSPROGS-make_install.log

chmod -v u+w /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a

gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info

makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info

cd /sources
rm -rf $E2FSPROGS

############ SYSKLOGD ############
echo $SYSKLOGD" start"

tar -xf $SYSKLOGD.tar.gz
cd $SYSKLOGD

sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
sed -i 's/union wait/int/' syslogd.c

echo $SYSKLOGD" config"
./configure --prefix=/usr > /logs/$SYSKLOGD-config.log

echo $SYSKLOGD" make"
make > /logs/$SYSKLOGD-make.log
echo $SYSKLOGD" make install"
make BINDIR=/sbin install > /logs/$SYSKLOGD-make_install.log

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF

cd /sources
rm -rf $SYSKLOGD

############ SYSVINIT ############
echo $SYSVINIT" start"

tar -xf $SYSVINIT.tar.xz
cd $SYSVINIT

patch -Np1 -i ../sysvinit-2.97-consolidated-1.patch

echo $SYSVINIT" make"
make > /logs/$SYSVINIT-make.log
echo $SYSVINIT" make install"
make install > /logs/$SYSVINIT-make_install.log

cd /sources
rm -rf $SYSVINIT
